package com.test.movieapp.service;

import java.util.List;

import com.test.movieapp.domain.Movie;



public interface MovieService {
	public void addMovie(Movie movie);

	public List<Movie> getMovie();
}
