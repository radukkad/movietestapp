package com.test.movieapp.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.test.movieapp.dao.MovieDao;
import com.test.movieapp.domain.Movie;
import com.test.movieapp.service.MovieService;



@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieDao movieDao;

	@Override
	public void addMovie(Movie movie) {
		movieDao.saveMovie(movie);
	}

	@Override
	public List<Movie> getMovie() {
		return movieDao.getMovie();
	}

}
