package com.test.movieapp.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.test.movieapp.domain.Movie;
import com.test.movieapp.service.MovieService;


@Controller
public class HomeController {

	
	@Autowired
	private MovieService movieService;
	
	@RequestMapping("/index")
	public ModelAndView getHome(@ModelAttribute("movie") Movie movie,
			BindingResult result,HttpServletRequest request) {
		System.out.println("Home Page");
		Object loggedIn = request.getSession().getAttribute("loggedin");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("loggedIn", loggedIn);
		return new ModelAndView("Index",model);
	}
	
	@RequestMapping("/addmovie")
	public ModelAndView getAddMovieForm(@ModelAttribute("movie") Movie movie,
			BindingResult result) {
		System.out.println("Add Movie form");
		Map<String, Object> model = new HashMap<String, Object>();
	    return new ModelAndView("AddMovie",model);
	}
	
	
	@RequestMapping(value="/savemovie", method=RequestMethod.POST)
	public ModelAndView saveMovie(@ModelAttribute("movie") Movie movie,
			BindingResult result,HttpServletRequest request) {
			movieService.addMovie(movie);
			return new ModelAndView("redirect:/movies.html");
	}
	
	@RequestMapping("/movies")
	public ModelAndView getMovies() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("movies", movieService.getMovie());
		return new ModelAndView("MovieList", model);
	}
	
	@PostConstruct
	public void init()
	{
		 System.out.println("Controller loaded");
	}
}
