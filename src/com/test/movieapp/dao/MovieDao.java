package com.test.movieapp.dao;

import java.util.List;

import com.test.movieapp.domain.Movie;

public interface MovieDao {
public void saveMovie ( Movie movie );
public List<Movie> getMovie();
}
