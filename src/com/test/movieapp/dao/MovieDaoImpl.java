package com.test.movieapp.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.movieapp.dao.MovieDao;
import com.test.movieapp.domain.Movie;


@Repository("movieDao")
public class MovieDaoImpl implements MovieDao {

	@Autowired
	private SessionFactory sessionfactory;

	@Override
	public void saveMovie(Movie movie) {
		sessionfactory.getCurrentSession().saveOrUpdate(movie);
	}

	@Override
	public List<Movie> getMovie() {
		@SuppressWarnings("unchecked")
		List<Movie> movielist = sessionfactory.getCurrentSession()
				.createCriteria(Movie.class).list();
		return movielist;
	}

}
