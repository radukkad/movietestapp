<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Movie Store</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<style>
table.gridtable {
      font-family: verdana,arial,sans-serif;
      font-size:13px;
      color:#333333;
      border-width: 1px;
      border-color: #666666;
      border-collapse: collapse;
    }
    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #dedede;
    }
    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #ffffff;
    }
    table.fixed { table-layout:fixed; }
	table.fixed td { overflow: hidden; }
	body {
    font-size: 62.5%;
    font-family: verdana,arial,sans-serif;
     font-size:13px;
}
</style>
</head>
<body>
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Movie List </h1>
			<c:if test="${!empty movies}">
			<table border="1" class="gridtable" width="50%">
              	<tr bgcolor="#00FF00">
                	<th>Movie Name</th>
                	<th>Description</th>
                	<th>Banner</th>
                	<th>Year</th>
               	</tr>
              	<c:forEach items="${movies}" var="movie">
				<tr>
             		<td>${movie.movieName}</td>
             		<td>${movie.movieDesc}</td>
             		<td>${movie.banner}</td>
             		<td>${movie.year}</td>
             	</tr>
				</c:forEach>
			</table>
			</c:if>
		</div>
	</div>
	<!-- end content -->
	<div style="clear: both;">&nbsp;</div>
</div>
</div>
</div>
<a href="addmovie.html">Add Movie</a> &nbsp; | &nbsp;<a href="index.html">Home</a> &nbsp;
</body>
</html>
