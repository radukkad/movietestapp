<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Add new movie </title>
<style>
body {
    font-size: 62.5%;
    font-family: verdana,arial,sans-serif;
     font-size:13px;
}
</style>
</head>
<body>
	<h1 class="title">Add New Movie </h1>
	<c:url var="saveMovie" value="savemovie"/>
	<form:form name="RegisterForm" id="RegisterForm" modelAttribute="movie" action="${saveMovie}" method="post" >
		Movie Name : <br/>
		<form:input id="movieName" path="movieName" value="" /><br/>
		Description : <br/>
		<form:textarea id="desc" path="movieDesc" value="" rows="4"  cols="20"/><br/>
		Banner : <br/>
		<form:input id="banner" path="banner" value="" /><br/>
		Year : <br/>
		<form:input id="year" path="year" value="" /><br/>
		</br>
		<input type="submit" id="Login" value="Add" />
	</form:form></br>
	<a href="movies.html">List Movies</a> &nbsp; | &nbsp;<a href="index.html">Home</a> &nbsp;
	</body>
</html>
